# Docker 101
What is docker - docker is a tool that helps us build containers

## Prerequisites
- install docker
- have it working on the CLI

## Main commands
Running our first container
```bash
docker run -d -p 80:80 docker/getting-started

# docker run [-options] owner_repo/image
    # -d detached / daemon (run in background)
    # -p port specify a mapping of a port (80:80 host:guest)
        # The host id is the physical machone you are running docker
        # the guest is the app inside the container

docker ps # shows docker processes running



docker kill <container id>
```

## Docker Hub!

https://hub.docker.com/

todo:

- What is docker hub?
- find the hello-word docker image
- download and have it running on port 80
- how do you give a running container a name?
  - run another container with hello-world call it "Bazinga" and make it run on port 4501





```bash

docker run -itd --name httpd_3 -P localhost:5000/httpd_3



docker build -t php-sql -f {sql-Dockerfile} .


docker run -d -name app-container-name --link mysql-container-name app-image-name

docker run -d -name app-container-name --link mysql-container-name app-image-name



#--volume=/root/docker/my_db/conf.d:/etc/mysql/conf.d
docker network create -d bridge mynetwork
docker run -itd --name my_db -p 3306:3306 --network mynetwork -e MYSQL_ROOT_PASSWORD=adminpassword mysql



docker build -t php-app -f DockerfilePHP .
docker run -itd --name phpapp --network mynetwork -P php-app
docker exec -it my_db mysql -h 127.0.0.1 -P 3306 -u root --password="adminpassword"
```