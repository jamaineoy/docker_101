<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>VeganMuffins</title>
    <link rel="shortcut icon" href="https://www.favicon.cc/logo3d/229023.png"/>
    <!-- <style>
        h1 { color: #111; font-family: 'Helvetica Neue', sans-serif; font-size: 50px; font-weight: bold; letter-spacing: -1px; line-height: 1; text-align: center; }
        .center {
                margin-left: auto;
                margin-right: auto;
                height: auto;
                width: auto;
                }

        .button {
        background-color: #FFFFFF; /* Green */
        border: none;
        color: black;
        padding: 15px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin-left: auto;
        margin-right: auto;
        }
    </style> -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body style="background-color:rgb(147, 197, 255);">
    <h1 class="center" style="font-size: 5vh;margin-left: auto;margin-right: auto;">JaSSy Vegan Muffin Store</h1>
    <img style="display: inline-block;" src="https://media4.giphy.com/media/H1MLSXznpkxn7P0GUx/200.gif">
    <iframe style="display: inline-block;" width="560" height="315"  src="https://www.youtube.com/embed/htp0LZBArM8?cc_load_policy=1&autoplay=1" title="YouTube video player" frameborder="0" allow="autoplay; accelerometer; clipboard-write; encrypted-media; gyroscope;picture-in-picture" allowfullscreen></iframe>
    <iframe src="https://open.spotify.com/embed/track/6tzPDA4TyPHZi4idbAs3Po" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
    <img style="display: inline-block;" src="https://media4.giphy.com/media/H1MLSXznpkxn7P0GUx/200.gif">
    <ul>
        <?php
            $json = file_get_contents('http://product-api');
            $objts = json_decode($json);
            $products = $objts->products;
            $json2 = file_get_contents('http://product-api/price');
            $objts2 = json_decode($json2);
            $prices = $objts2->prices;
            $len = count($products);
            for ($x = 0; $x < $len; $x++) {
                echo "<li style=\"font-size: 16px\"><a href=\"$products[$x].php\">$products[$x]</a></li>";
                }     
        ?>
    </ul>
</body>
</html>