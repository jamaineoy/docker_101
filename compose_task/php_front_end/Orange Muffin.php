<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Orange</title>
    <link rel="shortcut icon" href="https://www.favicon.cc/logo3d/229023.png"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>
    body {
        background-color: orange;
    }
    </style>
</head>
<body>
    <h1>Spectacular Orange Muffin</h1>
    <img src="https://d2wtgwi3o396m5.cloudfront.net/recipe/25041ce0-3e6c-4bd4-9bc8-a6b104c5ea2a.jpg?d=1408x1120" height="400px">
    <br><a href="index.php">Back to home</a>
</body>
</html>