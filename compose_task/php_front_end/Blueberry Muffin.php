<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Blueberry</title>
    <link rel="shortcut icon" href="https://www.favicon.cc/logo3d/229023.png"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>
    body {
        background-color: lightblue;
    }
    </style>
</head>
<body>
    <h1>Blueberry Muffin</h1>
    <img src="https://sweetcsdesigns.com/wp-content/uploads/2020/05/blueberry-muffins-Recipe-Picture-720x720.jpg" height="400px">
    <br><a href="index.php">Back to home</a>
</body>
</html>