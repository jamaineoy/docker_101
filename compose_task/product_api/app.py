from flask import Flask, render_template
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

class Product(Resource):
    def get(self):
        return {
            'products': ['Orange Muffin', 'Plain Old Muffin', 'Blueberry Muffin']
        }

        # return render_template('index.html', muffins=products)


class ProductPrice(Resource):
    def get(self):
        return {
            'prices': {
                'Orange Muffin': 10,
                'Plain Old Muffin': 17,
                'Blueberry Muffin': 12
            }
        }


api.add_resource(Product, '/')
api.add_resource(ProductPrice, '/price')



if __name__=="__main__":
    app.run(host='0.0.0.0',port=80, debug=True)